var SpControls = {};
SpControls.init = function(){
    // Colors
    $('.color-picker').colorSliders();
    $('.bg-color-picker').colorSliders({
        type: 'background-color'
    });
    $('.border-color-picker').colorSliders({
        type: 'border-color'
    });

    // FontFamily
    $('.font-selector').radioSelector({
        type: 'font-family',
        useUniform: false
    });

    // Font Sizes
    $('.number-slider-font').numberSlider({
        min: 8,
        max: 24,
        step: 1,
        units: 'px'
    });

    // Border width
    $('.number-slider-border').numberSlider({
        min: 0,
        max: 20,
        step: 1,
        units: 'px',
        type: 'border-width'
    });
// height
    $('.number-slider-height').numberSlider({
        min: 35,
        max: 100,
        step: 1,
        units: 'px',
        type: 'height'
    });
    // Border radius
    $('.number-slider-border-radius').numberSlider({
        min: 0,
        max: 20,
        step: 1,
        units: 'px',
        type: 'border-radius'
    });

    // Border styles
    $('.border-selector').radioSelector({
        type: 'border-style',
        useUniform: false
    });

    // Font styles
    $('.font-style').fontStyles();

    $('.color-picker-hover').colorSliders({
        subClass: 'hover'
    });

    $('.bg-color-picker-hover').colorSliders({
        type: 'background-color',
        subClass: 'hover'
    });
    $('.border-color-picker-hover').colorSliders({
        type: 'border-color',
        subClass: 'hover'
    });

    // Opacity
    $('.number-slider-opacity').numberSlider({
        min: 0,
        max: 100,
        step: 1,
        units: '%',
        type: 'opacity'
    });
    $('.opacity-hover').numberSlider({
        min: 0,
        max: 100,
        step: 1,
        units: '%',
        type: 'opacity',
        subClass: 'hover'
    });
};
