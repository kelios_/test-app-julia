<?php
/**
 * @var array $link
 * @var array $selector
 * @var array $form
 */
$scriptId = rand(10000, 99999);

if (!function_exists('_selector_picker_replaceDomainName')) {
    /**
     * @param string $url
     * @return string
     */
    function _selector_picker_replaceDomainName($url)
    {
        try {
            list($replaceWhat, $replaceTo) = _selector_picker_domains();
            if (!$replaceWhat || !$replaceTo || $replaceWhat === $replaceTo) {
                return $url;
            }
            return str_replace($replaceWhat . '/', $replaceTo . '/', $url);

        } catch (Exception $e) {
            return $url;
        }
    }

    /**
     * @return array
     */
    function _selector_picker_domains()
    {
        if (!method_exists('App', 'uses')) {
            return ['', ''];
        }

        App::uses('AuthComponent', 'Component');
        App::uses('Shop', 'Model');
        App::uses('ShopifyApi', 'Lib');

        if (!method_exists('AuthComponent', 'user') || !class_exists('Shop') || !method_exists('ShopifyApi', 'getShopInfo')) {
            return ['', ''];
        }

        $userId = AuthComponent::user()['id'];

        if (isset($_SESSION['selector_picker_domains-' . $userId]) && is_array($_SESSION['selector_picker_domains-' . $userId])) {
            return $_SESSION['selector_picker_domains-' . $userId];
        }
        $_SESSION['selector_picker_domains-' . $userId] = ['', ''];

        $oShop = new Shop();
        $shop = $oShop->findByUserId($userId);
        if (!isset($shop['Shop'])) {
            return ['', ''];
        }

        $oApi = new ShopifyApi($shop['Shop']['domain'], $shop['Shop']['token']);
        $info = $oApi->getShopInfo();
        if (!isset($info->domain)) {
            return ['', ''];
        }

        $_SESSION['selector_picker_domains-' . $userId] = [$info->myshopify_domain, $info->domain];
        return $_SESSION['selector_picker_domains-' . $userId];
    }
}

$url = $link['url'];
$url .= strpos($url, '?') === false ? '?' : '&';
if (isset($link['sign']) && trim($link['sign']) !== '') {
    $url .= 'sign=' . rawurlencode($link['sign']) . '&';
}
$url .= 'selector-picker=' . $scriptId . '&settings=' . rawurlencode(json_encode($selector));
$url = _selector_picker_replaceDomainName($url);
?>
<a href="<?= $url ?>" target="_blank" <?php if (isset($link['class'])): ?>class="<?= $link['class'] ?>"<?php endif ?>
   <?php if (isset($link['id'])): ?>id="<?= $link['id'] ?><?php endif ?>"><?= $link['text'] ?></a>
<script>
    $(function () {
        var scriptId = <?=$scriptId?>;

        function getElement(selector, title) {
            var $element = $(selector);
            if (!$element || !$element.length) {
                console.error('SelectorPicker: ' + title + ' "' + selector + '" not found');
                return null;
            }
            var tagName = $element.get(0).tagName;
            if (tagName !== 'INPUT' && tagName !== 'SELECT' && tagName !== 'TEXTAREA') {
                console.error('SelectorPicker: ' + title + ' "' + selector + '" should be an INPUT, SELECT or TEXTAREA');
                return null;
            }
            return $element;
        }

        function valueGet($element) {
            if ($element.length > 1 || $element.get(0).type === 'radio' || $element.get(0).type === 'checkbox') {
                return $element.filter(':checked').val();
            } else {
                return $element.val();
            }
        }

        function valueSet($element, value) {
            if ($element.length > 1 || $element.get(0).type === 'radio' || $element.get(0).type === 'checkbox') {
                $element.filter('[value="' + value + '"]').attr('checked', true);
            } else {
                $element.val(value);
            }
            var event = document.createEvent('HTMLEvents');
            event.initEvent('change', true, true);
            $element.get(0).dispatchEvent(event);
        }

        var $elementSelector, $elementPosition = null;
        $elementSelector = getElement('<?=$form['elementSelector']?>', 'element for writing selector');
        if (!$elementSelector) {
            return;
        }
        <?php if(isset($form['elementPosition'])):?>
        $elementPosition = getElement('<?=$form['elementPosition']?>', 'element for writing position');
        if (!$elementPosition) {
            return;
        }
        <?php endif?>

        $(window).bind('message', function (e) {
            var data = e.data || e.originalEvent.data;
            if (typeof(data.id) === 'undefined' || data.id !== scriptId) {
                return;
            }
            if (typeof(data.selector) !== 'undefined') {
                valueSet($elementSelector, data.selector);
            }
            if (typeof(data.position) !== 'undefined' && $elementPosition) {
                valueSet($elementPosition, data.position);
            }
            if (data.loadInitial) {
                var source = e.source || e.originalEvent.source;
                if ($elementPosition) {
                    source.postMessage({
                        id: scriptId, inital: {
                            selector: valueGet($elementSelector),
                            position: valueGet($elementPosition)
                        }
                    }, '*');
                } else {
                    source.postMessage({
                        id: scriptId, inital: {
                            selector: valueGet($elementSelector)
                        }
                    }, '*');
                }
            }
        });
    });
</script>