#!/usr/bin/php -q
<?php

$start = microtime(true);

define('STATICS_DIR', __DIR__ . '/..');
define('LIBRARY_DIR', __DIR__ . '/libs');

define('CONFIG_INI', __DIR__ . '/deploy.ini');

/********** Deployment config ***********/

$uploadStatics = true;
$compressStatics = true;

/****************************************/

include_once(LIBRARY_DIR . '/Deploy.php');

// Read INI config
$iniConfig = Deploy::readIniConfig(
    CONFIG_INI,
    ['amazon_bucket', 'amazon_key', 'amazon_secret', 'amazon_folder_js']
);


if ($uploadStatics) {
    // Upload statics
    include_once(LIBRARY_DIR . '/Statics.php');
    $oStatics = new Statics(
        $iniConfig['amazon_bucket'],
        $iniConfig['amazon_key'],
        $iniConfig['amazon_secret']
    );
    $oStatics->uploadJs(STATICS_DIR, $iniConfig['amazon_folder_js'], $compressStatics);
    Deploy::display('');
}

Deploy::display('Done at '.sprintf('%.02f', microtime(true) - $start).' sec');

exit(0);