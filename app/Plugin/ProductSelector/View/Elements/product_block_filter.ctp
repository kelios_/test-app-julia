<div class="g_6 <?php echo $platform ?> product-selector-filter">
	<div class="widget_contents noPadding">
		<?php if ( $enableTitle ) : ?>
			<div class="line_grid">
				<label class="g_6">
					<span class="label">Filter by Title</span>
				</label>
				<div class="g_6">
					<input type="text" name="filter[text]" class="simple_field" id="<?php echo $leftBlockId ?>-filterTitle" />
					<?php /*if ( strtolower( $platform ) == 'bigcommerce' ) : ?>
						<div class="field_notice">
							Please enter full title of the product/category. It is not allowed to search by parts of the title using Bigcommerce API.
						</div>
					<?php endif */ ?>
				</div>
			</div>
		<?php endif ?>
		<?php if ( $enableCategories ) : ?>
			<div class="line_grid" id="<?php echo $leftBlockId ?>-filter-categories">
				<?php echo $this->Form->input( 'collection_id', array (
					'type' => 'select',
					'options' => array (
						@$collections
					),
					'escape' => false,
					'empty' => 'All products',
					'between' => '<div class="g_6">',
					'after' => '</div>',
					'class' => 'simple_form',
					'id' => $leftBlockId . '-filterCategory',
					'label' => array (
					'class' => 'g_6',
						'text' => '<span class="label">Filter by ' . ( strtolower( $platform ) == 'shopify' ? 'Collections' : 'Categories' ) . '</span>'
					)
				) ) ?>
			</div>
		<?php endif ?>
		<?php if ( strtolower( $platform ) == 'shopify' ) : ?>
			<div class="line_grid" id="<?php echo $leftBlockId ?>-group-wrappper">
				<div class="g_12">
					<label class="label">
						<input type="hidden" name="<?php echo $checkboxName ?>" value="0" />
						<input id="<?php echo $leftBlockId ?>-group" class="simple_form" type="checkbox" name="<?php echo $checkboxName ?>" value="1" <?php if ( $checkboxChecked ) : ?>checked="checked" <?php endif ?>/> Group product variants
					</label>
				</div>
			</div>
		<?php endif ?>
		<div class="line_grid">
			<input id="<?php echo $leftBlockId ?>-fab" class="simple_buttons filter-apply-btn" type="button" value="Search" />
		</div>
	</div>
</div>
