<ul><?php

	$index = 0;
	foreach ( $categories as $category ) {
		$inputHidden = '
		<input type="hidden" name="'. $rightBlockId .'['. $index .'][cid]" value="'. $category->id .'" />
		<input type="hidden" name="'. $rightBlockId .'['. $index .'][handle]" value="'. $category->handle .'" />
		';
		$index++;
		echo "
		<li id=\"dc-{$category->id}\">
			<a href=\"#\" class=\"remove\" title=\"remove category\" onclick=\"$( this ).parent().parent().parent().data( '_this' ).removeFromDiscount( {$category->id}, 'category' ); return false;\"></a>
			$inputHidden
			<div class=\"title\">
				<a href=\"http://{$shop}{$category->handle}\" target=\"_blank\">
					<strong>{$category->title}</strong>
				</a>
			</div>
			<div class=\"clear\"></div>
		</li>
		";
	}

	foreach ( $products as $product ) {
		// Image.
		$src = $product->images ?: 'no-image-thumb.gif';
		$image = $this->Html->image( $src, array (
		    'class' => 'product-selector-thumb',
			'alt' => 'thumb',
			'width' => '50'
		) );
		// Price.
		$price = str_replace( '{{amount}}', $product->variants[0]->price, $moneyFormat );
		$comparePrice = (float) $product->variants[0]->compare_at_price ? str_replace( '{{amount}}', $product->variants[0]->compare_at_price, $moneyFormat ) : '';
		// Separate variants.
		foreach ( $product->variants as $variant ) {
			$inputHidden = '
			<input type="hidden" name="'. $rightBlockId .'['. $index .'][pid]" value="'. $product->id .'" />
			<input type="hidden" name="'. $rightBlockId .'['. $index .'][vid]" value="'. $variant->id .'" />
			<input type="hidden" name="'. $rightBlockId .'['. $index .'][handle]" value="'. $product->handle .'" />
			';
			$index++;
			echo "
			<li id=\"dp-{$variant->id}\">
				<a href=\"#\" class=\"remove\" title=\"remove product\" onclick=\"$( this ).parent().parent().parent().data( '_this' ).removeFromDiscount( {$variant->id}, 'product' ); return false;\"></a>
				$inputHidden $image
				<div class=\"title\">
					<a href=\"http://{$shop}{$product->handle}\" target=\"_blank\">
						<strong>{$product->title}</strong>
					</a>
				</div>
				<div class=\"price\"><span>{$comparePrice}</span>{$price}</div>
				<div class=\"clear\"></div>
			</li>
			";
		}
	}

?></ul>
