<?php

/**
 * Cart model
 *
 * @author SpurIT <contact@spur-i-t.com>
 */
App::uses('AppModel', 'Model');

/**
 * Class Cart
 *
 */
class CartReminder extends AppModel {

    public $useTable = 'cart_reminders';
    public $time_param = [
        '1' => 'seconds',
        '2' => 'minutes',
    ];
    public $position_bar = [
        '1' => 'At the top (page content pushed down)',
        '2' => 'At the top (page content covered)',
        '3' => 'At the bottom (page content covered)'
    ];

    public $shop_id;

}
