<?php

class CartReminderController extends AppController {
    /*
      Show setting for cart reminder
     */

    public function admin() {
        $shop = $this->getShopInDb();
        try {
            $catrreminder = $this->CartReminder;
            if (!empty($shop)) {
                $catrreminder->shop_id = $shop['Shop']['id'];

                $this->request->data = $catrreminder->find('first', array(
                    'conditions' => array('CartReminder.shop_id' => $catrreminder->shop_id)
                ));
            }

            $this->set('currentPage', 'Cart Reminder Setting');
            $this->set(array(
                'catrreminder' => $catrreminder,
            ));
        } catch (Exception $e) {
            $this->addToLog('Error: uninstallation failed' . "\n" . 'Error message: "' . $e->getMessage() . '".');
            return false;
        }
    }

    /*
      Save setting for cart reminder
     */

    public function edit($id = null) {
        $shop = $this->getShopInDb();
        try {
            if (!empty($shop)) {
                $this->set('admin', 'Cart Reminder Setting');

                if ($this->request->is(array('post', 'put'))) {
                    try {
                        if ($this->CartReminder->save($this->request->data)) {
                            $shopHash = $this->Shop->getHash($shop);
                            $this->_setConfig(json_encode($this->request->data), $shopHash);
                            $this->setFlashMessage('Cart Reminder have been saved successfully.');
                        } else {
                            $this->setFlashMessage('Unable to save cart reminder. Please try again.', 'error');
                        }
                    } catch (Exception $e) {
                        $this->setFlashMessage($e->getMessage(), 'error');
                    }
                }
            }
        } catch (Exception $e) {
            $this->addToLog('Error: uninstallation failed' . "\n" . 'Error message: "' . $e->getMessage() . '".');
            return false;
        }

        $this->redirect(array('action' => 'admin'));
    }

    /*
      set config for Shopify json format
     */

    protected function _setConfig($data, $shopHash) {
        $data_conf = 'Spurit.juliaCartReminder.conf = ' . $data;
        $fp = fopen(WWW_ROOT . "/files/upload/" . $shopHash . ".js", "w");
        fwrite($fp, $data_conf);
        fclose($fp);

        $this->Amazon->upload(
                Configure::read('AppConf.amazonAPI.folderStore') . $shopHash . '.js', $data_conf, 'application/javascript'
        );
    }

    /*
      Set log for cart reminder
     */

    protected function addToLog($message) {
        $this->log($message, 'cartreminder');
    }

}
