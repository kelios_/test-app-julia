<?php
class SettingsController extends AppController
{
	public $components = array('ProductSelector.Product');

	public function edit()
    {
        $this->set('currentPage', 'Widget Customization');
        $shop = $this->getShopInDb();
        if (empty($shop['Shop']['settings'])) $shop['Shop']['settings'] = Configure::read('AppConf.default_settings');

        if ($this->request->is(array('post', 'put')))
        {
            $this->Shop->id = $shop['Shop']['id'];
            try{
                if ($this->Shop->save($this->request->data))
                {
                    $targets = isset($this->request->data['Target'])? $this->request->data['Target'] : array();
                    if($this->Target->saveTargets($targets, $shop['Shop']['id'])){
                        $this->uploadShopConfig($shop['Shop']['id']);
                        $this->setFlashMessage('Settings have been saved successfully.');
                    }
                    else $this->setFlashMessage('Unable to save target products. Please try again.', 'error');
                }
                else $this->setFlashMessage('Unable to save settings. Please try again.', 'error');
            }
            catch(Exception $e){
                $this->setFlashMessage($e->getMessage(), 'error');
            }
        }
        else $this->request->data = array('Shop' => $shop['Shop'], 'Target' => $this->Target->getTargets($shop['Shop']['id']));

        $firstAvailable = $this->getFirstProduct($this->api);
        $this->set(
            'selectorPickingURL',
            'https://' . $shop['Shop']['domain'] . '/products/' . $firstAvailable[0]->handle . '?variant=' . $firstAvailable[1]->id
        );
		$this->_setShopifyInfo(array('fields' => 'money_format'));
		$this->set(array(
            'productCount' => $this->api->get('products/count'),
            'collections' => $this->Product->getCollections(),
            'shopName' => $shop['Shop']['domain'],
            'data' => $this->request->data,
            'shopifyInfo' => $this->_getShopifyInfo(),
            'ajaxUrl' => 'getProductsFromRequest'
        ));
	}

    /**
     * Styles Editor (for Popup)
     * Actions on style save: save it in db, regenerate CSS file and upload it to Amazon S3.
     */
    public function design()
    {
        $this->set('currentPage', 'Design');
        $shop = $this->getShopInDb();
        $style = $shop['Shop']['style'];
        if (empty($style)) $style = Configure::read('AppConf.default_styles');

        if ($this->request->is('get')) {
            $this->request->data['Shop']['style'] = $style;
        } else {
            $data = array('Shop' => array('id' => $shop['Shop']['id'], 'style' => $this->request->data['Shop']['style']));
            if ($this->Shop->save($data) && $this->uploadShopCSS($shop['Shop']['id'])) {
                $this->setFlashMessage(__('Styles have been saved successfully'));
                $this->redirect(array('action' => 'design'));
            } else {
                $this->setFlashMessage(__('An error occurred, please try again later.', 'error'));
            }
        }
    }

    /**
     * Load default style and save it for the shop.
     * After resetting new CSS file will be generated and uploaded to S3.
     */
    public function reset_style()
    {
        try{
            $shopId = $this->getShopId();
            $response = $this->Shop->resetStyles($shopId);
        } catch(Exception $e) {
            $response = false;
        }

        if ($response && $this->uploadShopCSS($shopId)) {
            $this->setFlashMessage(__('Styles has been reset successfully'));
        } else {
            $this->setFlashMessage(__('An error occurred, please try again later.', 'error'));
        }

        $this->redirect(array('action' => 'design'));
    }

    /**
     * Update liquid templates of the main theme of the shop.
     */
    public function update_templates()
    {
        $this->setApiParams($this->shopInDb);

        if ($this->requestAction(
            array('controller' => 'setup', 'action' => 'reinstall_templates'),
            array('data' => array('shop'=>$this->shopInDb['Shop']['domain'], 'token'=>$this->shopInDb['Shop']['token']))
        ))
        {
            $this->setFlashMessage(__('Main theme templates have been updated.'));
        } else {
            $this->setFlashMessage(__('An error occurred, please try again later.'), 'error');
        }
        $this->redirect(array('controller' => 'settings', 'action' => 'edit'));
    }

	/**
     * prepare selected products list for each model
     * @internal AJAX
     */
    public function getProductsFromRequest($modelName = 'Target')
    {
        $this->autoRender = false;
        $productsHtml = '';
        $this->Product->setConfig( array('shopName' => $this->shopInDb[ 'Shop' ][ 'domain' ]) );
        $pids = $vids = $cids = array();
        $data = $this->request->data( 'postParams' );

        if ( isset($data[$modelName]) )
        {
            foreach ( $data[ $modelName ] as $_product )
            {
                if (isset($_product[ 'pid' ])) $pids[ ] = $_product[ 'pid' ];
                if (isset($_product[ 'vid' ])) $vids[ ] = $_product[ 'vid' ];
                if (isset($_product[ 'cid' ])) $cids[ ] = $_product[ 'cid' ];
            }
        }

        $pids = array_unique( $pids );
        if ( !in_array( 0, $pids ) )
        {
            $selectedProducts = $this->Product->loadShopifySelectedProducts( $pids, $vids );
            $categories = $this->Product->loadShopifySelectedCategories( $cids );
            $productsHtml = $this->setSelectedProducts( $selectedProducts, $categories, $modelName );
        }

        return $productsHtml;
    }

    /**
     * set selected products to view
     * @param $selectedProducts
     * @param $selectedCategories
     * @param $modelName
     * @internal AJAX
     * @return string
     */
    protected function setSelectedProducts( $selectedProducts, $selectedCategories, $modelName )
    {
    	$this->_setShopifyInfo(array('fields' => 'money_format'));
        $this->set(
            array(
                'moneyFormat' => $this->_shopifyInfo[ 'money_format' ],
                'products' => $selectedProducts[0],
                'variants' => $selectedProducts[1],
                'rightBlockId' => $modelName,
                'group' => true,
                'categories' => $selectedCategories,
                'shop' => $this->shopInDb[ 'Shop' ][ 'domain' ],
                'platform' => strtolower( Configure::read( 'AppConf.plugins.platform' ) )
            )
        );

        $this->render('selected_shopify_products', 'ajax');
    }

    /**
     * @param ShopifyApi $api
     * @return array
     */
    protected function getFirstProduct(ShopifyApi $api)
    {
        $products = $api->get(
            'products',
            null,
            [
                'fields'           => 'handle,variants',
                'published_status' => 'published',
                'limit'            => 50
            ]
        );

        foreach ($products as $product) {
            foreach ($product->variants as $variant) {
                if($variant->inventory_quantity > 0 || $variant->inventory_policy === 'continue' || $variant->inventory_management === null){
                    return [$product, $variant];
                }
            }
        }

        return [null, null];
    }
}