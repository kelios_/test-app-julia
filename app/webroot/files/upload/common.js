(function () {

    /**
     * @param {string|[string]} url  One or more urls
     * @param {function()} callback
     * @param {string} [type]
     */
    function loadStatic(url, callback, type) {
        if (typeof (callback) !== 'function') {
            callback = function () {};
        }
        if (url.constructor.name === 'Array') {
            var loadedCnt = 0;
            for (var i = 0, l = url.length; i < l; i++) {
                loadStatic(
                        url[i],
                        function () {
                            loadedCnt++;
                            if (loadedCnt === url.length) {
                                callback();
                            }
                        },
                        type
                        );
            }
        } else {
            if (type !== 'js' && type !== 'css') {
                type = url.toLowerCase().split('?')[0].split('#')[0].split('.').pop();
            }
            if (type !== 'js' && type !== 'css') {
                console.error('Undefined type of static file "' + url + '"');
                callback();
            }
            var tag;
            if (type === 'js') {
                tag = document.createElement('script');
                tag.type = 'text/javascript';
            } else {
                tag = document.createElement('link');
                tag.type = 'text/css';
                tag.rel = 'stylesheet';
            }
            if (tag.readyState) {// If the browser is Internet Explorer.
                tag.onreadystatechange = function () {
                    if (tag.readyState === 'loaded' || tag.readyState === 'complete') {
                        tag.onreadystatechange = null;
                        callback();
                    }
                };
            } else { // For any other browser.
                tag.onload = function () {
                    callback();
                };
            }
            if (type === 'js') {
                tag.src = url;
            } else {
                tag.href = url;
            }
            document.getElementsByTagName('head')[0].appendChild(tag);
        }
    }

    /*
     * SpuritLoc.cartReminder obj
     * Spurit.juliaCartReminder.shopHash
     */
    if (typeof (SpuritLoc) === 'undefined') {
        var SpuritLoc = {
            cartReminder: {
                config: {
                    s3path: 'https://s3.amazonaws.com/shopify-apps/julia_app_test',
                },
                countProduct: 0,
                interval_message: {1: 1000, 2: 1000 * 60},
                positionbar: {1: 'top', 2: 'topfixed', 3: 'bottom'},
                time: '',
                interval_tab: 500,
                shoptitle: document.title,
            }
        };

        SpuritLoc.cartReminder.static_style = {1:SpuritLoc.cartReminder.config.s3path + '/css/common.css'};

    }

    /*
     * 
     * Init cart reminder
     */

    SpuritLoc.init = function ($) {
        SpuritLoc.getConfig();
    }

    /* 
     * Show Tab
     */
    SpuritLoc.ShowTab = function () {
        var i = 0;
        var show = ['************', SpuritLoc.cartReminder.config.message_tab];

        var focusTimer = setInterval(function () {
            document.title = show[i++ % 2];
        }, SpuritLoc.cartReminder.interval_tab);

        document.onmousemove = function () {
            clearInterval(focusTimer);
            document.title = SpuritLoc.cartReminder.shoptitle;
            document.onmousemove = null;
        }

    }

    /* 
     * Show Bar
     */
    SpuritLoc.ShowBar = function () {
        var bar = document.createElement('div');
        var pos = SpuritLoc.cartReminder.config.position_bar;
        bar.innerHTML = '<div class="spuritcartreminder_bar ' + SpuritLoc.cartReminder.positionbar[pos] + '">' + SpuritLoc.cartReminder.config.message_bar + '</div>';
        if (undefined !== $('.spuritcartreminder_bar') && !$('.spuritcartreminder_bar').length)
            document.body.prepend(bar);
    }



    /*Get interval show bar
     * 
     * @returns {time|int}
     */

    SpuritLoc.getTime = function () {
        var timereminder = SpuritLoc.cartReminder.config.time_description;
        SpuritLoc.cartReminder.time = SpuritLoc.cartReminder.config.time * SpuritLoc.cartReminder.interval_message[timereminder];
    }


    /* Get count in cart(api cart.js)
     * set countProduct
     */
    SpuritLoc.getCountInCart = function () {

       /*  SpuritLoc.cartReminder.countProduct = 1;
          SpuritLoc.CartReminder();*/
        $.getJSON('/cart.js', function (cart) {
            SpuritLoc.cartReminder.countProduct = cart.item_count;
            SpuritLoc.CartReminder();
        });
    }

    /* Show message on bar and tab
     * 
     */
    SpuritLoc.CartReminder = function () {
        if (SpuritLoc.cartReminder.countProduct >= 1) {
            console.log();
            $.each(SpuritLoc.cartReminder.static_style, function (index, value) {
                loadStatic(value);
            });


            if (SpuritLoc.cartReminder.config.status_plugin > 0) {
                if (SpuritLoc.cartReminder.config.status_plugin) {
                    SpuritLoc.getTime();
                    setTimeout(SpuritLoc.ShowTab, SpuritLoc.cartReminder.time);
                    setTimeout(SpuritLoc.ShowBar, SpuritLoc.cartReminder.time);
                }
            }
        }
    }

    /*
     * 
     * @returns {config for plugin}
     */

    SpuritLoc.getConfig = function () {

        loadStatic(SpuritLoc.cartReminder.config.s3path + '/store/' + Spurit.juliaCartReminder.shopHash + '.js?' + Math.random(), function () {
            SpuritLoc.setConfig(Spurit.juliaCartReminder.conf.CartReminder);
            SpuritLoc.getCountInCart();
        });

    }


    /* Set config reminder
     * 
     CartReminder":{    "id":"1",
     "shop_id":"864",
     "status_plugin":"1",
     "message_tab":"test",
     "message_bar":"test",
     "time":"1111",
     "time_description":"2",
     "status_bar":"1",
     "position_bar":"2"
     }
     */

    SpuritLoc.setConfig = function (conf) {
        SpuritLoc.cartReminder.config = conf;
    }

    /*================================================== JQUERY CHECKING ==================================================*/

    if (typeof jQuery === 'undefined') {
        loadStatic('//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js', function () {
            SpuritLoc.init(jQuery);
        });
    } else {
        SpuritLoc.init(jQuery);
    }
})();

CartReminderconf = function (json) {
    console.log(json);
}


