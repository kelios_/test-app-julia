(function(){
/*================================================== CODE FOR SELECTOP PICKER ==================================================*/
    "use strict"
    
    var selectorPicker = {
        url: 'https://s3.amazonaws.com/shopify-apps/Plugins/SelectorPicker/selector-picker.js',
        cmd: 'sign=spurit_hub'
    };

    /**
     * @param {string|[string]} url  One or more urls
     * @param {function()} callback
     * @param {string} [type]
     */
    function loadStatic(url, callback, type) {
        if (typeof(callback) !== 'function') {
            callback = function () {};
        }
        if (url.constructor.name === 'Array') {
            var loadedCnt = 0;
            for (var i = 0, l = url.length; i < l; i++) {
                loadStatic(
                    url[i],
                    function () {
                        loadedCnt++;
                        if (loadedCnt === url.length) {
                            callback();
                        }
                    },
                    type
                );
            }
        } else {
            if (type !== 'js' && type !== 'css') {
                type = url.toLowerCase().split('?')[0].split('#')[0].split('.').pop();
            }
            if (type !== 'js' && type !== 'css') {
                console.error('Undefined type of static file "' + url + '"');
                callback();
            }
            var tag;
            if (type === 'js') {
                tag = document.createElement('script');
                tag.type = 'text/javascript';
            } else {
                tag = document.createElement('link');
                tag.type = 'text/css';
                tag.rel = 'stylesheet';
            }
            if (tag.readyState) {// If the browser is Internet Explorer.
                tag.onreadystatechange = function () {
                    if (tag.readyState === 'loaded' || tag.readyState === 'complete') {
                        tag.onreadystatechange = null;
                        callback();
                    }
                };
            } else { // For any other browser.
                tag.onload = function () {
                    callback();
                };
            }
            if (type === 'js') {
                tag.src = url;
            } else {
                tag.href = url;
            }
            document.getElementsByTagName('head')[0].appendChild(tag);
        }
    }

    // Display selector picker if necessary
    if (document.location.search.indexOf(selectorPicker.cmd) !== -1) {
        loadStatic(selectorPicker.url);
        return;
    }

/*================================================== COMMON FUNCTIONS ==================================================*/

    function randomInteger(min, max) {
        var rand = min - 0.5 + Math.random() * (max - min + 1);
        rand = Math.round(rand);
        return rand;
    }

/*================================================== APP ==================================================*/

    var app = {
        config: {
            folder: 'social-proof',
            prefix: 'HUB'
        }
    };
    app.waitingForAJAX = false;
    app.sessionData = '';
    app.config.s3Prefix = '//s3.amazonaws.com/shopify-extensions/' + app.config.folder;

    app.init = function($){
        $('head').append('<link rel="stylesheet" href="'+ app.config.s3Prefix + '/css/common.css" type="text/css" />');
        $('head').append('<link rel="stylesheet" href="'+ app.config.s3Prefix + '/store/' + HUBParams.id + '.css?' + Math.random() + '" type="text/css" />');

        if (!Object.assign) {
          Object.defineProperty(Object, 'assign', {
            enumerable: false,
            configurable: true,
            writable: true,
            value: function(target, firstSource) {
              'use strict';
              if (target === undefined || target === null) {
                throw new TypeError('Cannot convert first argument to object');
              }

              var to = Object(target);
              for (var i = 1; i < arguments.length; i++) {
                var nextSource = arguments[i];
                if (nextSource === undefined || nextSource === null) {
                  continue;
                }

                var keysArray = Object.keys(Object(nextSource));
                for (var nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
                  var nextKey = keysArray[nextIndex];
                  var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
                  if (desc !== undefined && desc.enumerable) {
                    to[nextKey] = nextSource[nextKey];
                  }
                }
              }
              return to;
            }
          });
        }

        loadStatic(app.config.s3Prefix + '/store/' + HUBParams.id + '.js?' + Math.random(), function(){
            Object.assign(app.config, HUBParams, HUBConfig);
            if (typeof HUBCustom != 'undefined') Object.assign(app.config, HUBCustom);
            if (app.getConfig('fake_visitors') == 0) app.getSessionData();
            app.run($);
        });
    };
    
    app.getSessionData = function(){
        if (app.sessionData === ''){
            var obj = { url: window.location.href }
            if ( !(obj.user_id = window.sessionStorage.getItem('hub_id')) ){
                obj.user_id = Math.random().toString(36).slice(1);
                window.sessionStorage.setItem('hub_id', obj.user_id);
            }
            app.sessionData = JSON.stringify(obj);
        }
        return app.sessionData;
    };
  
    app.getLastRandStock = function(){
        return JSON.parse(window.sessionStorage.getItem('hub_stock'));
    };

    app.isTargetProduct = function(product){
        var targets = this.getConfig('targets');
        if (Object.keys(targets).length === 0) return true;

        var len = targets.length;
        for(var i=0; i<len; i++){
            if (typeof targets[i].cid !== 'undefined'){
                if (product.getCollectionIds().indexOf(+targets[i].cid) != -1) return true;
            }
            else if (product.getId() == targets[i].pid) return true;
        }

        return false;
    };

    app.getConfig = function(key, subkey){
        if (key){
            if (typeof subkey !== 'undefined'){
                return (typeof this.config[key][subkey] !== 'undefined')? this.config[key][subkey] : null;
            }
            else return (typeof this.config[key] !== 'undefined')? this.config[key] : null;
        }
        else return this.config;
    };

    app.getVisitors = function(){
        if (typeof this.visitors === 'undefined'){
            if (this.getConfig('fake_visitors') === '1'){
                this.visitors = randomInteger(this.getConfig('rand_visitors', 'min'), this.getConfig('rand_visitors', 'max'));
            }
            else{
                try{
                    this.waitingForAJAX = true;
                    $.ajax(
                        'https://pv-app.spur-i-t.com/register',
                        {
                            type: 'post',
                            data: app.sessionData,
                            contentType: 'application/json',
                            error: function(data){
                                app.visitors = randomInteger(1,10);
                                throw new Error('Unable to recieve visitors count. Server response: ' + data.statusText);
                            },
                            success: function(data){
                                app.visitors = data.number;
                            },
                            complete: function(){
                                app.waitingForAJAX = false;
                            }
                        }
                    ); 
                }
                catch(e){
                    console.log('Error: ' + e.message);
                }
            }
        }
        return this.visitors;
    };
    
    app.checkVisitors = function(){ return typeof app.visitors !== 'undefined'; };

    app.run = function($){
        var product = new Product(app.getConfig('product'));
        if ( !product.id ) throw new Error('There is no product on the page.');

        if (this.isTargetProduct(product)){
            var stock = product.getRealStock() && product.getStock();
            this.getVisitors();

            window.addEventListener('beforeunload', function(){
                if (app.getConfig('fake_visitors') === '0'){
                    $.ajax(
                        'https://pv-app.spur-i-t.com/unregister',
                        {
                            type: 'POST',
                            data: app.sessionData,
                            contentType: 'application/json',
                            async: false
                        }
                    );
                }
            });
            
            this.waitData(this.showMessage, this, [stock]);
        }
    };
    
    app.showMessage = function(stock){
        var msgBox = new MessageBox(stock, app.getVisitors());
        if (msgBox.checkConditions()){
            var sel = app.getConfig('selector');
            var sibling = $(sel).first();
            if ( !sibling.length ){
                var timeout = app.getConfig('timeout') || 1000;
                setTimeout(function(){
                    var sel = app.getConfig('selector');
                    var sibling = $(sel).first();
                    if ( !sibling.length ) throw new Error('There is no element with selector ' + sel);
                    app.appendContainer(msgBox, sibling);
                }, 1000);
            }
            else app.appendContainer(msgBox, sibling);
        }
    };
    
    app.waitData = function(callback, obj, args){
        var cycle = setInterval(function(){
            if (!app.waitingForAJAX){
                clearInterval(cycle);  
                callback.apply(obj, args);
            }
        }, 200);
    }

    app.appendContainer = function(msgBox, sibling)
    {
        var box = msgBox.getContainer();
        if ($(box).children('#hub-left').length){
            if (app.getConfig('position') == 'after') $(sibling).after(msgBox.getContainer());
            else $(sibling).before(msgBox.getContainer());
        }
    }

/*================================================== PRODUCT ==================================================*/

    var Product = function(product)
    {
        this.stock;
        this.realStock;
        this.id = product.id;
        this.collectionIds;
        this.getId = function(){ return this.id; };

        this.getStock = function(){
            if (typeof this.stock === 'undefined'){
                if (typeof this.realStock === 'undefined'){
                    this.realStock = 0;
                    for(var i=0; i<product.variants.length; i++){
                        if (product.variants[i].inventory_management != 'shopify'){
                            if (app.getConfig('fake_stock') === '1') this.realStock = true;
                            else this.realStock = false;
                            break;
                        }
                        this.realStock += product.variants[i].inventory_quantity;
                    }
                }
                if ( !product.available || (this.realStock < 1 && app.getConfig('showStockWhenPOEnabled') === null) ){
                    return this.realStock;
                }

                if (app.getConfig('fake_stock') === '1'){
                    var stockObject = app.getLastRandStock() || {};
                    if (typeof stockObject[this.id] !== 'undefined') this.stock = stockObject[this.id];
                    else{
                        this.stock = randomInteger(app.getConfig('rand_stock', 'min'), app.getConfig('rand_stock', 'max'));
                        stockObject[this.id] = this.stock;
                        window.sessionStorage.setItem('hub_stock', JSON.stringify(stockObject));
                    }   
                }
                else this.stock = this.realStock;
            }
          
            return this.stock;
        };

        this.getRealStock = function(){
            if (typeof this.realStock === 'undefined') this.getStock();
            return this.realStock;
        };
        
        this.getCollectionIds = function(){
            if (typeof this.collectionIds === 'undefined'){
                this.collectionIds = [];
                for(var i=0; i<product.collections.length; i++) this.collectionIds.push(product.collections[i].id);
            }
            return this.collectionIds;
        };
    };

/*================================================== MESSAGE BOX ==================================================*/

    var MessageBox = function(stock, visitors){
        this.stock = stock;  
        this.visitors = visitors;
        this.messages = undefined;

        this.checkConditions = function(){
            var initial = {};
            var initStock = parseInt(app.getConfig('initial_stock'), 10);
            var initVisitors = parseInt(app.getConfig('initial_visitors'), 10);

            if (!isNaN(initStock)) initial.stock = initStock;
            if (!isNaN(initVisitors)) initial.visitors = initVisitors;
            var objLen = Object.keys(initial).length;

            if (objLen !== 0){
                var key;
                if (app.getConfig('operator') == '1'){
                    var j=0;
                    for(key in initial){
                        if (this[key] === false) break;
                        if (initial[key] > this[key]) j++;
                    }
                    if (j == objLen) return false;
                }
                else{
                    for(key in initial){
                        if (initial[key] > this[key] && this[key] !== false) return false;
                    }
                }
            }
            
            return true;
        };

        this.getContent = function(){
            if (typeof this.messages === 'undefined'){
                var allMsgs = app.getConfig('messages');
                this.messages = {
                    one: this.getMsg(allMsgs, 'visitors', this.visitors, 1)
                };

                if (this.stock > 0) this.messages.two = this.getMsg(allMsgs, 'quantity', this.stock, 1);

                if (this.messages.one && this.messages.two){
                    this.messages.three = this.getMsg(allMsgs, 'hurry_up', this.stock + this.visitors, 2);
                }
                else this.messages.three = null;
                
                this.messages = this.parseMessages(this.messages, this.stock, this.visitors);
              
                if (this.messages.three) this.messages.icon = this.getIcon(this.stock, this.visitors);
                else this.messages.icon = null;
            }

            return this.messages;
        };

        this.getMsg = function(msgs, key, variable, compare){
            var msgKey;
            if (variable == compare) msgKey = 'alt_' + key;
            else msgKey = key;

            if (app.getConfig('switches', key) == '1' && msgs[msgKey] !== '') return msgs[msgKey];
            else return null;
        };

        this.getIcon = function(stock, visitors){
            var key;
            if (stock == 1 && visitors == 1) key = 'alt_icon';
            else key = 'icon';

            var icon = app.getConfig(key);
            return icon || null;
        };

        this.parseMessages = function(msgs, stock, visitors){
            for(var key in msgs){
                if (msgs[key]) msgs[key] = msgs[key].replace(/\[number\]/g, visitors).replace(/\[quantity\]/g, stock);
            }

            return msgs;
        };

        this.getContainer = function(){
            var container = document.createElement('div');
            $(container).attr('id', 'hub-container');
            var msgs = this.getContent();

            if (msgs.one || msgs.two)
            {
                var left = document.createElement('div');
                $(left).attr('id', 'hub-left');
                if (msgs.one) $(left).append('<span>' + msgs.one + '</span>');
                if (msgs.two) $(left).append('<span>' + msgs.two + '</span>');
                $(container).append(left);

                if (msgs.three){
                    if (msgs.icon){
                        var icon = document.createElement('div');
                        $(icon).attr('id', 'hub-middle');
                        $(icon).html('<span>' + msgs.icon + '</span>');
                        $(container).append(icon);
                    }
                    var right = document.createElement('div');
                    $(right).attr('id', 'hub-right');
                    $(right).append('<span>' + msgs.three + '</span>');
                    $(container).append(right);
                }
            }

            return container;
        };
    };

/*================================================== JQUERY CHECKING ==================================================*/

    if ( typeof jQuery === 'undefined' ) {
        loadStatic('//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js', function(){
            app.init(jQuery);
        });
    } else {
        app.init(jQuery);
    }
})();

