<?php
/**
 * Provides interaction with PrestaShop API.
 *
 * @author Kovalev Yury
 * @copyright 2012 SpurIT <contact@spur-i-t.com>, All rights reserved.
 * @link http://spur-i-t.com
 * @version 1.0.1
 */
class PrestaShop extends AbstractApi {

	protected $apiName = 'PrestaShop';

	protected $apiUrl = 'http://%s/api/%s';

	protected $parseXML = true;

	protected $apiResouces = array(
		'products', 'images/products'
	);

	/**
	 * Constructor.
	 * Initializes api.
	 * @param string $shop - shop name.
	 * @param string $apiKey - api key of shop.
	 * @return PrestaShop
	 */
	public function __construct($shop, $apiKey) {
		parent::__construct();
		$this->init( $shop, $apiKey );
		return $this;
	}

	/**
	 * Initializes api.
	 * @param string $shop - shop name.
	 * @param string $apiKey - api key of shop.
	 * @return PrestaShop
	 */
	public function init( $shop, $apiKey )
	{
		$this->shop = $shop;
		curl_setopt($this->client, CURLOPT_USERPWD, $apiKey . ':');
		return $this;
	}

	/**
	 * Returns parse flag
	 * @return bool
	 */
	public function getParseXmlFlag() {
		return $this->parseXML;
	}

	/**
	 * Sets parse flag for response content
	 * @param $value
	 */
	public function setParseXml($value) {
		$this->parseXML = $value;
	}

	/**
	 * Overload parent method
	 * Sends HTTP request to API server and returns response.
	 * @param string $method
	 * @param string $url
	 * @param string $format
	 * @param null $fields
	 * @return object|void
	 */
	public function request( $method, $url, $format, $fields = null )
	{
		// Remove json extension from request string
		$url = str_replace('.json', '', $url);

		return parent::request($method, $url, $format, $fields);
	}

	/**
	 * Overload parent method
	 * Adds id to resource.
	 * @param string $resource - resource name.
	 * @param integer $id - main item id.
	 */
	protected function _addResourceId( &$resource, $id )
	{
		if ( $id ) {
			$resource .= '/' . $id;
		}
	}

	/**
	 * Overload parent method
	 * Response content needn't to decode
	 * @param $response - response content
	 * @return mixed
	 */
	protected function _decodeResponse($response) {
		return $response;
	}

	/**
	 * @see AbstractAPI::_processResponse()
	 */
	protected function _processResponse($response, $responseHeaders) {
		$code = curl_getinfo($this->client, CURLINFO_HTTP_CODE);

		// check the response validity
		$this->checkStatusCode($code);

		$codeFirstNumber = substr($code, 0, 1);
		if (isset ($response->errors)) {
			$errors = json_encode($response->errors);
			throw new Exception(
				$this->_message($errors)
			);
		} else if (($codeFirstNumber == 4) || ($codeFirstNumber == 5)) {
			throw new Exception(
				$this->_message($response[0]->message)
			);
		}
		if ($this->getParseXmlFlag()) {
			$response = self::parseXML($response);
		}

		return $response;
	}

	/**
	 * @see AbstractAPI::_processNoResponse()
	 */
	protected function _processNoResponse() {
		$code = curl_getinfo($this->client, CURLINFO_HTTP_CODE);
		if ($code != 204) {
			throw new Exception(
				$this->_message('No response: ' . $code)
			);
		}
		return true;
	}

	/**
	 * @see AbstractAPI::_getMethodResponse()
	 */
	protected function _getMethodResponse($resource, $response, $idBunch) {
		if (isset ($response->count)) {
			$response = $response->count;
		}
		return $response;
	}


	/**
	 * Take the status code and throw an exception if the server didn't return 200 or 201 code
	 * @param int $status_code Status code of an HTTP return
	 * @throws Exception
	 */
	protected function checkStatusCode($status_code) {
		$error_label = 'This call to PrestaShop Web Services failed and returned an HTTP status of %d. That means: %s.';
		switch ($status_code) {
			case 200:
			case 201:
				break;
			case 204:
				throw new Exception(sprintf($error_label, $status_code, 'No content'));
				break;
			case 400:
				throw new Exception(sprintf($error_label, $status_code, 'Bad Request'));
				break;
			case 401:
				throw new Exception(sprintf($error_label, $status_code, 'Unauthorized'));
				break;
			case 404:
				throw new Exception(sprintf($error_label, $status_code, 'Not Found'));
				break;
			case 405:
				throw new Exception(sprintf($error_label, $status_code, 'Method Not Allowed'));
				break;
			case 500:
				throw new Exception(sprintf($error_label, $status_code, 'Internal Server Error'));
				break;
			default:
				throw new Exception('This call to PrestaShop Web Services returned an unexpected HTTP status of:' . $status_code);
		}
	}

	/**
	 * Load XML from string. Can throw exception
	 * @param string $response String from a CURL response
	 * @return SimpleXMLElement status_code, response
	 * @throws Exception
	 */
	protected function parseXML($response) {
		if ($response != '') {
			libxml_use_internal_errors(true);
			$xml = simplexml_load_string($response);
			if (libxml_get_errors()) {
				throw new Exception('HTTP XML response is not parsable : ' . var_export(libxml_get_errors(), true));
			}

			return $xml;
		} else {
			throw new Exception('HTTP response is empty');
		}
	}
}
