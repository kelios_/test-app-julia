<?php
	/**
	 * Provides interaction with BigCommerce API.
	 *
	 * @author Polyakov Ivan
	 * @copyright 2012 SpurIT <contact@spur-i-t.com>, All rights reserved.
	 * @link http://spur-i-t.com
	 * @version 2.0.2
	 */
	class BigCommerceNew extends AbstractApi
	{
		protected $apiName = 'Bigcommerce';

		protected $apiUrl = 'https://api.bigcommerce.com/stores/%s/v2/%s';

		protected $apiResouces = array (
			'brands', 'categories', 'customers', 'customers/addresses', 'customer_groups', 'coupons', 'countries', 'countries/states',
			'orders', 'orders/coupons', 'orders/products', 'orders/shipments', 'orders/shippingaddresses', 'options', 'options/value', 'optionsets', 'optionsets/options', 'orderstatuses',
			'products', 'products/skus', 'products/configurablefields', 'products/customfields', 'products/discountrules', 'products/images', 'products/options', 'products/rules', 'products/videos',
			'redirects', 'shipping/methods', 'store', 'time', 'hooks'
		);

		/**
		 * Constructor.
		 * Initializes api.
		 * @param string $storeHash - store hash.
		 * @param string $clientId - client id.
		 * @param string $token - token.
		 * @return BigCommerceNew
		 */
		public function __construct( $storeHash, $clientId, $token )
		{
			parent::__construct();
			$this->init( $storeHash, $clientId, $token );
			return $this;
		}

		/**
		 * Initializes api.
		 * @param string $storeHash - store hash.
		 * @param string $clientId - client id.
		 * @param string $token - token.
		 * @return BigCommerceNew
		 */
		public function init( $storeHash, $clientId, $token )
		{
			$this->shop = $storeHash;
			$this->headers = array (
				"X-Auth-Client: {$clientId}",
				"X-Auth-Token: {$token}"
			);
			$this->responseHeaders = true;
			curl_setopt( $this->client, CURLOPT_HEADER, true );
			return $this;
		}

		/**
		 * Add a custom header to the request.
		 * @param $header - key
		 * @param $value - value
		 */
		public function addHeader( $header, $value )
		{
			$this->headers[] = "{$header}: {$value}";
		}

        /**
         * Get store information
         * @return mixed
         */
		public function getShop()
        {
            return $this->get('store');
        }

		/**
		 * @see AbstractAPI::_formGetResource()
		 */
		protected function _formGetResource( $resource, $count, $format, $params )
		{
			$url = "{$resource}{$count}";
			if ( $params ) {
				$url .= '?' . http_build_query( $params );
			}
			return $url;
		}

		/**
		 * @see AbstractAPI::_formPostResource()
		 */
		protected function _formPostResource( $resource, $format )
		{
			$url = "{$resource}";
			return $url;
		}

		/**
		 * @see AbstractAPI::_formPutResource()
		 */
		protected function _formPutResource( $resource, $format )
		{
			$url = "{$resource}";
			return $url;
		}

		/**
		 * @see AbstractAPI::_formDeleteResource()
		 */
		protected function _formDeleteResource( $resource, $format, $params = null )
		{
			$url = "{$resource}";
			return $url;
		}

		/**
		 * @see AbstractAPI::_processResponse()
		 */
		protected function _processResponse( $response, $responseHeaders )
		{
			$code = curl_getinfo( $this->client, CURLINFO_HTTP_CODE );
            while($code == 429) {
                if ( isset($responseHeaders['X-Retry-After']) ) {
                    $pause = $responseHeaders['X-Retry-After'];
                } else {
                    $pause = 5;
                }
                CakeLog::write('bc_api', 'Shop domain: ' . $this->shop . '. Error: (429)Request limit reached, sleep ' . $pause . 's...');
                sleep($pause);
                if ( $response = curl_exec( $this->client ) ) {
                    $responseHeaders = array ();
                    if ( $this->responseHeaders ) {
                        $response = explode( "\n", $response );
                        foreach ( $response as $line ) {
                            if ( strlen( trim( $line ) ) ) {
                                if ( strstr( $line, ':' ) ) {
                                    $line = explode( ':', $line );
                                    $responseHeaders[ $line[0] ] = trim( $line[1] );
                                } else {
                                    $responseHeaders[] = $line;
                                }
                            }
                            else break;
                        }
                        $response = array_pop( $response );
                    }
                    $response = $this->_decodeResponse( $response );
                    $code = curl_getinfo( $this->client, CURLINFO_HTTP_CODE );
                } else {
                    return $this->_processNoResponse();
                }
            }

			$codeFirstNumber = substr( $code, 0, 1 );
			if ( isset ( $response->errors ) ) {
				$errors = json_encode( $response->errors );
				throw new ApiException(
					$this->_message( $errors ), null, $code
				);
			} else if ( ( $codeFirstNumber == 4 ) || ( $codeFirstNumber == 5 ) ) {
				throw new ApiException(
					$this->_message( json_encode( $response ) ),
                    null,
                    $code
				);
			}
			return $response;
		}

		/**
		 * @see AbstractAPI::_processNoResponse()
		 */
		protected function _processNoResponse()
		{
			$code = curl_getinfo( $this->client, CURLINFO_HTTP_CODE );
			if ( $code >= 400 ) {
				throw new ApiException(
					$this->_message( 'No response, ' . $code )
				);
			}
			return false;
		}

		/**
		 * @see AbstractAPI::_getMethodResponse()
		 */
		protected function _getMethodResponse( $resource, $response, $idBunch )
		{
			if ( isset ( $response->count ) ) {
				$response = $response->count;
			}
			return $response;
		}
	}
