<?php
class PluginCenterApi
{
	private $key;
	private $secret;
	private $client;

	private $headers = array(
		'Content-Type' => "Content-Type: application/json; charset=utf-8",
		'Accept' => "Accept: application/json",
	);

	public function __construct($apiKey, $apiSecret)
	{
		$this->key = $apiKey;
		$this->secret = $apiSecret;
		$this->client = curl_init();
		curl_setopt( $this->client, CURLOPT_FOLLOWLOCATION, true );
		curl_setopt( $this->client, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $this->client, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $this->client, CURLOPT_SSL_VERIFYHOST, 0 );
	}

	public function __destruct()
	{
		curl_close( $this->client );
	}

	protected function __clone() {}

	public function setUrl($url)
	{
		curl_setopt( $this->client, CURLOPT_URL, $url );
		return $this;
	}

	public function setBody($body)
	{
		if ( $body ) {
			$fields = json_encode( $body );
			$this->headers['Content-Length'] = "Content-Length: " . strlen( $fields );
			curl_setopt( $this->client, CURLOPT_POSTFIELDS, $fields );
		}
		return $this;
	}

	public function setMethod($method)
	{
		curl_setopt( $this->client, CURLOPT_CUSTOMREQUEST, strtoupper( $method ) );
		return $this;
	}

	public function setHeaders($headers = array())
	{
		curl_setopt( $this->client, CURLOPT_HTTPHEADER, array_merge( $headers, $this->headers ) );
		return $this;
	}

	public function setBackground($background)
	{
		if($background){
			curl_setopt($this->client, CURLOPT_FRESH_CONNECT, true);
			curl_setopt($this->client, CURLOPT_TIMEOUT_MS, 1);
		}
		return $this;
	}

	public function connect($url, $method = 'GET', $body = array(), $background = false)
	{
		$message = '';
		$this->setUrl($url)
			->setMethod($method)
			->setCredentials($url)
			->setBody($body)
			->setBackground($background)
			->setHeaders();
		$response = curl_exec( $this->client );
		$code = curl_getinfo( $this->client, CURLINFO_HTTP_CODE );
		if ( $response ) {
			$data = json_decode($response);
			if(!empty($data->error))
				throw new PluginCenterException('Http '.$code.': '.$data->error);

			if(!empty($data->message))
				$message = $data->message;
		} elseif(!$background && ($code >300)) {
			throw new PluginCenterException('Http '.$code.': Empty response');
		}
		return $message;
	}

	public function setCredentials($url)
	{
		$this->headers['Pc-Api-Key'] = "Pc-Api-Key: {$this->key}";
		$token = $this->getToken($url, $this->secret);
		$this->headers['Pc-Api-Token'] = "Pc-Api-Token: {$token}";
		return $this;
	}

	protected function getToken($url, $secret)
	{
		return trim(base64_encode(hash_hmac(
			"sha1",
			$url,
			$secret,
			true
		)));
	}
}
