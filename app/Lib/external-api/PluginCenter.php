<?php
	/**
	 * Plugin Center interface.
	 * Provides with common interface all plugins of different platforms for interaction with Plugin Center.
	 * It must be initialized before using.
	 *
	 * @author Polyakov Ivan
	 * @package Application
	 * @copyright 2012 SpurIT <contact@spur-i-t.com>, All rights reserved.
	 * @link http://spur-i-t.com
	 * @version 1.3.1
	 */
	class PluginCenter
	{
		/** Module name.
		 * @var string
		 */
		static private $_module;

		/** Controller name.
		 * @var string
		 */
		static private $_controller;

		/** Action name.
		 * @var string
		 */
		static private $_action;

		/** Shop platform.
		 * @var string
		 */
		static private $_platform;

		/** User id.
		 * @var string
		 */
		static private $_user;

		/** Shop name.
		 * @var string
		 */
		static private $_shop;

		/** Plan name.
		 * @var string
		 */
		static private $_plan;

		/** Plugin name.
		 * @var string
		 */
		static private $_plugin;

		/** Command line's output.
		 * @var string
		 */
		static private $_output;

		private function __clone() {}

		private function __construct() {}

		/**
		 * Initialization.
		 * @param array $params
		 */
		static public function init( array $params )
		{
			if ( isset ( $params['platform'] ) ) {
				self::$_platform = $params['platform'];
			}
			if ( isset ( $params['user'] ) ) {
				self::$_user = $params['user'];
			}
			if ( isset ( $params['shop'] ) ) {
				self::$_shop = $params['shop'];
			}
			if ( isset ( $params['plugin'] ) ) {
				self::$_plugin = $params['plugin'];
			}
		}

		/**
		 * Connects with PC through command line and calls given action.
		 * Returns command line output.
		 *
		 * @param array $actionParams - action parameters.
		 * @param bool $bg - Background mode. Doesn't return anything.
		 * @return string
		 */
		static private function _connect( $actionParams = null, $bg = false )
		{
			// Form params.
			$actionParams['platform'] = self::$_platform;
			$actionParams['plugin'] = self::$_plugin;
			if ( self::$_user ) {
				$actionParams['user'] = self::$_user;
			}
			if ( self::$_shop ) {
				$actionParams['shop'] = self::$_shop;
			}
			$params = "-m ". self::$_module ." -c ". self::$_controller ." -a ". self::$_action;
			if ( $actionParams ) {
				foreach ( $actionParams as $_name => $_value ) {
					$params .= " --{$_name} '{$_value}'";
				}
			}
			// Enable bg mode.
			if ( $bg ) {
				$bg = '> /dev/null 2>&1 &';
			}
			// Execute
			self::$_output = '';
			$pcConfig = self::_getPcConfig();
			$execPath = $pcConfig['pc_exec_path'];
			// Run index file of pc.
			$command = "php -f {$execPath} -- {$params}{$bg}";
			// If pc is located on remote server, connect via ssh and run file.
			if ( isset ( $pcConfig['pc_is_remote'] ) && $pcConfig['pc_is_remote'] ) {
				$command = 'ssh -l '.$pcConfig ['pc_user'] .' -p '.$pcConfig ['pc_port']. ' '.$pcConfig ['pc_host'] .' '. $command;
			}
			exec( $command, self::$_output );
			return implode( "\n", self::$_output );
		}

		/**
		 * @return mixed
		 */
		static protected function _getPcConfig()
		{
			Configure::load( 'pc' );
			$pcConfig = Configure::read('pc');
			return $pcConfig;
		}

		/**
		 * Checks out whether required parameters are passed.
		 * @param array $parameters
		 * @throws Exception
		 */
		static private function _checkParameters( array $parameters )
		{
			foreach ( $parameters as $_param ) {
				$name = '_'.$_param;
				if ( !self::$$name ) {
					throw new Exception( 'Not all parameters were passed.' );
				}
			}
		}

		/**
		 * For AuthController.
		 * @param string $action - controller action.
		 * @param array $params - action parameters.
		 * @return string
		 */
		static public function auth( $action, $params = array () )
		{
			self::_checkParameters( array (
				'platform', 'user', 'plugin'
			) );
			self::$_module = self::$_platform;
			self::$_controller = 'auth';
			self::$_action = $action;
			return self::_connect( $params );
		}

		/**
		 * For CronController.
		 * @param string $action - controller action.
		 * @param array $params - action parameters.
		 * @return string
		 */
		static public function cron( $action, $params = array () )
		{
			self::$_module = 'default';
			self::$_controller = 'cron';
			self::$_action = $action;
			return self::_connect( $params );
		}

		/**
		  * For FinanceController.
		  * @param string $action - controller action.
		  * @param array $params - action parameters.
		  * @return string
		  */
		static public function finance( $action, $params = array () )
		{
			self::_checkParameters( array (
				'platform', 'user', 'plugin'
			) );
			self::$_module = "default";
			self::$_controller = 'finance';
			self::$_action = $action;
			return self::_connect( $params );
		}

		/**
		 * For OptionController.
		 * @param string $action - controller action.
		 * @param array $params - action parameters.
		 * @return string
		 */
		static public function options( $action, $params = array () )
		{
			self::_checkParameters( array (
				'platform', 'user', 'plugin'
			) );
			self::$_module = "default";
			self::$_controller = 'option';
			self::$_action = $action;
			return self::_connect( $params, true );
		}

		/**
		 * For PaymentController.
		 * @param string $action - controller action.
		 * @param array $params - action parameters.
		 * @return string
		 */
		static public function payment( $action, $params = array () )
		{
			self::_checkParameters( array (
				'platform', 'user', 'plugin'
			) );
			self::$_module = 'payment';
			self::$_controller = 'index';
			self::$_action = $action;
			return self::_connect( $params );
		}

		/**
		 * For PluginController.
		 * @param string $action - controller action.
		 * @param array $params - action parameters.
		 * @return string
		 */
		static public function plugin( $action, $params = array () )
		{
			self::_checkParameters( array (
				'platform', 'shop', 'plugin'
			) );
			self::$_module = self::$_platform;
			self::$_controller = 'plugin';
			self::$_action = $action;
			return self::_connect( $params );
		}

		/**
		 * For TemplateController.
		 * @param string $action - controller action.
		 * @param array $params - action parameters.
		 * @return string
		 */
		static public function template( $action, $params = array () )
		{
			self::_checkParameters( array (
				'platform', 'user', 'plugin'
			) );
			self::$_module = 'default';
			self::$_controller = 'template';
			self::$_action = $action;
			return self::_connect( $params );
		}



		/**
		 * send request to PC with report about email
		 * @param $params
		 * @author Kuksanau Ihnat
		 */
		static public function email($params){
			self::_checkParameters( array (
				'platform', 'plugin'
			) );

			self::$_module = 'default';
			self::$_controller = 'email';
			self::$_action = 'addemail';

			//prepare data for PC
			if(!isset($params['emailTo']))
				throw new Exception("\"emailTo\" is a mandatory param");

			if(!isset($params['emailFrom']))
				throw new Exception("\"emailFrom\" is a mandatory param");

			if(isset($params['emailNameFrom'])){
				$params['emailNameFrom'] = base64_encode($params['emailNameFrom']);
			}

			if(isset($params['emailSubject']) && isset($params['emailMessage'])){
				$params['emailSubject'] =  base64_encode($params['emailSubject']);
				$params['emailMessage'] = base64_encode(gzcompress( $params['emailMessage'] ));
			}else{
				throw new Exception("\"emailSubject\" or \"emailMessage\" were not passed");
			}

			//return url for hook onEmailSent from PC
			if(isset($params['callbackUrl'])){
				$params['callbackUrl'] = base64_encode( str_replace( ' ', '-', strtolower($params['callbackUrl']) ) );
			}

			self::_connect( $params, true );
		}


		/**
		 * register webhooks for domain via PC
		 * @param $params
		 * @throws Exception
		 * @author Kuksanau Ihnat
		 */
		static public function registerWebhooks($params)
		{
			self::_checkParameters( array (
				'platform', 'plugin', 'user'
			) );
			self::$_module = 'webhooks';
			self::$_controller = 'app';
			self::$_action = 'register';

			if(isset($params['webhooksData'])){
				$params['webhooksData'] =  base64_encode( json_encode($params['webhooksData']) );
			}else{
				throw new Exception("\"webhooksData\" param can not be empty");
			}

			if(!isset($params['shop'])){
				throw new Exception("\"shop\" param can not be empty");
			}

			self::_connect( $params, true );
		}

		/**
		 * @param $dependencyName
		 * @return string
		 */
		static public function isPluginInstalled( $dependencyName )
		{
			self::_checkParameters( array (
				'platform', 'user'
			) );
			self::$_module = 'default';
			self::$_controller = 'installs';
			self::$_action = 'is-installed';
			return self::_connect( array('plugin' => $dependencyName) );
		}
	}
