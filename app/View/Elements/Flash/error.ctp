<script>
    ShopifyApp.ready(function(){
        ShopifyApp.flashError("<?php echo h($message) ?>");
    });
</script>
<div id="<?php echo h($key) ?>Message" class="flash-message error"><?php echo h($message) ?></div>
