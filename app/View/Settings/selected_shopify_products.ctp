<ul><?php

	$index = 1;
    $counter = 1;
    if ( !empty($categories) ) {
        foreach ( $categories as $category ) {
            $inputHidden = '
		<input type="hidden" name="'. $rightBlockId .'['. $index .'][cid]" value="'. $category->id .'" />
		<input type="hidden" name="'. $rightBlockId .'['. $index .'][handle]" value="'. $category->handle .'" />
        <input type="hidden" name="'. $rightBlockId .'['. $index .'][pid]" value="" disabled />
		';
            $index++;
            echo "
		<li id=\"dc-{$category->id}\">
			<a href=\"#\" class=\"remove\" title=\"remove category\" onclick=\"$( this ).parent().parent().parent().data( '_this' ).removeFromDiscount( {$category->id}, 'category' ); return false;\"></a>
			$inputHidden
			<div class=\"title\">
				<a href=\"http://{$shop['Shop']['domain']}/collections/{$category->handle}\" target=\"_blank\">
					<strong>{$category->title}</strong>
				</a>
			</div>
			<div class=\"clear\"></div>
		</li>
		";
        }
    }

    if ( !empty($products) ) {
        foreach ( $products as $product ) {
            // Image.
            $src = 'no-image-thumb.gif';
            if ( isset ( $product->images[0] ) ) {
                $src = substr_replace(
                    $product->images[0]->src,
                    '_thumb'. substr( $product->images[0]->src, strrpos( $product->images[0]->src, '.' ) ),
                    strrpos( $product->images[0]->src, '.' )
                );
            }
            $image = $this->Html->image( $src, array (
                'class' => 'product-selector-thumb',
                'alt' => 'thumb',
                'width' => '50'
            ) );
            // Price.
            //$price = str_replace( '{{amount}}', $product->variants[0]->price, $moneyFormat );
            //$price = str_replace( '{{amount_no_decimals}}', $product->variants[0]->price, $price );
            $price = strip_tags($this->Money->formatMoney($moneyFormat, $product->variants[0]->price));
            $comparePrice = $product->variants[0]->compare_at_price ?
                strip_tags($this->Money->formatMoney($moneyFormat, $product->variants[0]->compare_at_price)): '';
                //str_replace('{{amount_no_decimals}}', $product->variants[0]->compare_at_price, str_replace( '{{amount}}', $product->variants[0]->compare_at_price, $moneyFormat )) :
            // Grouped variants.
            if ( $group ) {
                $inputHidden = '<input type="hidden" name="'. $rightBlockId .'['. $index .'][pid]" value="'. $product->id .'" />';
                $index++;
                $variantsHtml = $product->variants[0]->title;
                echo '
			<li id="dp-'. $product->variants[0]->id .'">
				<a href="#" class="remove" title="remove product" onclick="$( this ).parent().parent().parent().data( \'_this\' ).removeFromDiscount( '. $product->variants[0]->id .', \'product\' ); return false;"></a>
				' . $inputHidden . $image . '
				<div class="title">
					<a href="http://'. $shop['Shop']['domain'] .'/products/'. $product->handle .'" target="_blank"><strong>'. $product->title .'</strong></a>
				</div>
				<div class="price"><span>'. $comparePrice .'</span>'. $price .'</div>
				<div class="clear"></div>
			</li>
			';
            }
            // Separate variants.
            else {
                foreach ( $product->variants as $variant ) {
                    $inputHidden = '
				<input type="hidden" name="'. $rightBlockId .'['. $index .'][pid]" value="'. $product->id .'" />
				<input type="hidden" name="'. $rightBlockId .'['. $index .'][vid]" value="'. $variant->id .'" />
				';
                    $index++;
                    if ( !in_array( $variant->id, $variants ) ) continue;
                    echo "
				<li id=\"dp-{$variant->id}\">
					<a href=\"#\" class=\"remove\" title=\"remove product\" onclick=\"$( this ).parent().parent().parent().data( '_this' ).removeFromDiscount( {$variant->id}, 'product' ); return false;\"></a>
					$inputHidden $image
					<div class=\"title\"><a href=\"http://{$shop['Shop']['domain']}/products/{$product->handle}\" target=\"_blank\"><strong>{$product->title}</strong></a><br />{$variant->title}</div>
					<div class=\"price\"><span>{$comparePrice}</span>{$price}</div>
					<div class=\"clear\"></div>
				</li>
				";
                }
            }
        }
    }
?></ul>