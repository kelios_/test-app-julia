<?php
App::uses('AppHelper', 'View');

/**
 * Errors helper
 */
class ErrorsHelper extends AppHelper {
    
    public function error($fields)
    {
        $errors = array();
        foreach($fields as $field) {
            if (isset($this->data['errors'][$field])) {
                if (is_array($this->data['errors'][$field])) {
                    $message = implode('<br />', $this->data['errors'][$field]);
                } else {
                    $message = $this->data['errors'][$field];
                }
                $errors[] = $message;
            }
        }
        
        return '<div>' . implode('<br />', $errors) . '</div>';
    }
}
